using System.Collections;
using System.Collections.Generic;

using UnityEngine;

public class BobbleBehaviour : MonoBehaviour
{
    public float BobbleForce = 5f;
    public float BobbleFrequency = 1.5f;
    public float Variety = 1f;

    private RectTransform _rectTransform;

    void Start()
    {
        _rectTransform = GetComponent<RectTransform>();
        StartCoroutine(BobbleLoop(Random.Range(0, Variety)));
    }

    IEnumerator BobbleLoop(float variety)
    {
        while (true)
        {
            _rectTransform.localEulerAngles = new Vector3(_rectTransform.localEulerAngles.x, Mathf.Lerp(-BobbleForce, BobbleForce, Mathf.SmoothStep(0f, 1f, (Mathf.Sin((Time.time + variety) * BobbleFrequency) / 2.5f) + 0.5f)), _rectTransform.localEulerAngles.z);
            yield return null;
        }
    }
}
