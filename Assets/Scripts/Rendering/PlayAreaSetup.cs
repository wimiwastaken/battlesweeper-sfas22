using System.Collections;
using System.Collections.Generic;

using UnityEngine;
using UnityEngine.UI;

[RequireComponent(typeof(RectTransform))]
public class PlayAreaSetup : MonoBehaviour
{
    public float PlayAreaOffset;
    public Transform CameraUpperLimit, CameraLowerLimit;

    float _aspectRatio;

    private void Awake()
    {
        _aspectRatio = Camera.main.aspect;
        
        GetComponent<AspectRatioFitter>().aspectRatio = _aspectRatio;
        RectTransform rt = GetComponent<RectTransform>();
        rt.anchoredPosition3D = new Vector3(0, 0, PlayAreaOffset);
        rt.sizeDelta = new Vector2(0, ((CameraUpperLimit.position.z - PlayAreaOffset) - (CameraLowerLimit.position.z + PlayAreaOffset)) * 10);
    }
}
