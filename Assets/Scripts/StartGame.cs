using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StartGame : MonoBehaviour
{
    public UI UserInterface;

    public void LoadMenu()
    {
        UserInterface.ShowMenu();
    }
}
