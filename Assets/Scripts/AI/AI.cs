using System.Linq;
using System.Collections;
using System.Collections.Generic;

using UnityEngine;

public enum UncertainBool
{
    FALSE, MAYBE, TRUE
}

public class AI : MonoBehaviour
{
    public float WaitTime = 1.5f;
    
    internal Board PlayBoard;

    private int[] _riskGrid;
    private UncertainBool[] _estimatedDangersPlacementGrid;

    public void CalculateRiskGrid(bool firstRound = false)
    {
        _riskGrid = new int[PlayBoard._grid.Length];
        if (firstRound)
        {
            _estimatedDangersPlacementGrid = new UncertainBool[PlayBoard._grid.Length];
            for (int i = 0; i < _estimatedDangersPlacementGrid.Length; i++)
            {
                _estimatedDangersPlacementGrid[i] = UncertainBool.FALSE;
            }
        }
        for (int i = 0; i < _riskGrid.Length; i++)
        {
            _riskGrid[i] = firstRound ? -1 : CheckTileRisk(i);
        }
    }

    public IEnumerator PlayTurn()
    {
        if (_riskGrid.Where(x => x != 0).Max() == -1)
        {
            yield return new WaitForSeconds(WaitTime);
            ClickBox(Random.Range(0, _riskGrid.Length));
        }
        else
        {
            int safestTileIndex = -1;
            EstimateDanger();

            Dictionary<int, UncertainBool> uncertainBoolDictionary = _estimatedDangersPlacementGrid.Select((ubool, index) => new { ubool, index }).ToDictionary(x => x.index, x => x.ubool);
            List<int> limitedSafeIndexList = new List<int>(uncertainBoolDictionary.Where(x => x.Value == UncertainBool.FALSE).ToDictionary(x => x.Key, x => x.Value).Keys).Where(x => _riskGrid[x] == -1).ToList();

            foreach (int boxIndex in new List<int>(uncertainBoolDictionary.Where(x => x.Value == UncertainBool.TRUE).ToDictionary(x => x.Key, x => x.Value).Keys).Where(x => _riskGrid[x] == -1).ToList()) if (!PlayBoard._grid[boxIndex].IsFlagged) PlayBoard._grid[boxIndex].MarkFlag();


            if (limitedSafeIndexList.Any())
            {
                safestTileIndex = limitedSafeIndexList[Random.Range(0, limitedSafeIndexList.Count)];
                print("SMART SAFE PRESS: " + safestTileIndex);
                yield return new WaitForSeconds(WaitTime);
                ClickBox(safestTileIndex);
            }
            else
            {
                limitedSafeIndexList = new List<int>(uncertainBoolDictionary.Where(x => x.Value == UncertainBool.MAYBE).ToDictionary(x => x.Key, x => x.Value).Keys).Where(x => _riskGrid[x] == -1).ToList();
                if (limitedSafeIndexList.Any())
                {
                    safestTileIndex = limitedSafeIndexList[Random.Range(0, limitedSafeIndexList.Count)];
                    print("SMART RISK PRESS: " + safestTileIndex);
                    yield return new WaitForSeconds(WaitTime);
                    ClickBox(safestTileIndex);
                }
                else
                {
                    print("RANDOM PRESS");
                    yield return new WaitForSeconds(WaitTime);
                    Dictionary<int, int> riskDictionary = _riskGrid.Select((risk, index) => new { risk, index }).ToDictionary(x => x.index, x => x.risk);
                    List<int> limitedRiskList = new List<int>(riskDictionary.Where(x => x.Value == -1).ToDictionary(x => x.Key, x => x.Value).Keys);
                    ClickBox(limitedRiskList[Random.Range(0, limitedRiskList.Count)]);
                }
            }
        }
    }

    int CheckTileRisk(int gridIndex)
    {
        /* Key:
         * 9: A mine
         * 1 - 8: Near this amount of mines
         * 0: Not a mine
         * -1: Unpressed **/

        if (PlayBoard._grid[gridIndex].IsActive) return -1;
        if (PlayBoard._grid[gridIndex].IsUnknown) return 10;
        return PlayBoard._grid[gridIndex].DangerNearby;
    }

    void EstimateDanger()
    {
        for (int gridIndex = 0; gridIndex < _riskGrid.Length; gridIndex++)
        {
            if (_riskGrid[gridIndex] > -1 && _riskGrid[gridIndex] != 10)
            {
                _estimatedDangersPlacementGrid[gridIndex] = UncertainBool.FALSE;
            }
            else
            {
                _estimatedDangersPlacementGrid[gridIndex] = UncertainBool.MAYBE;
            }
        }

        Dictionary<int, UncertainBool> uncertainBoolDictionary = _estimatedDangersPlacementGrid.Select((ubool, index) => new { ubool, index }).ToDictionary(x => x.index, x => x.ubool);
        List<int> limitedSafeIndexList = new List<int>(uncertainBoolDictionary.Where(x => x.Value == UncertainBool.FALSE).ToDictionary(x => x.Key, x => x.Value).Keys);

        Vector2Int[] neighbourTileIndices = new Vector2Int[8]
        {
            new Vector2Int(-PlayBoard._gridWidth - 1, -1),
            new Vector2Int(-PlayBoard._gridWidth, -1),
            new Vector2Int(-PlayBoard._gridWidth + 1, -1),
            new Vector2Int(-1, 0),
            new Vector2Int(1, 0),
            new Vector2Int(PlayBoard._gridWidth - 1, 1),
            new Vector2Int(PlayBoard._gridWidth, 1 ),
            new Vector2Int(PlayBoard._gridWidth + 1, 1)
        };

        foreach(int index in limitedSafeIndexList)
        {
            Dictionary<int, int> neighbourRisk = new Dictionary<int, int>();

            for (int i = 0; i < neighbourTileIndices.Length; i++)
            {
                int neighbourIndex = index + neighbourTileIndices[i].x;
                int expectedRow = (index / PlayBoard._gridWidth) + neighbourTileIndices[i].y;
                int neighbourRow = neighbourIndex / PlayBoard._gridWidth;

                if (expectedRow == neighbourRow && neighbourIndex >= 0 && neighbourIndex < _riskGrid.Length && !neighbourRisk.ContainsKey(neighbourIndex))
                {
                    neighbourRisk.Add(neighbourIndex, _riskGrid[neighbourIndex]);
                }
            }

            if(neighbourRisk.Where(x => x.Value == -1).Where(x => _estimatedDangersPlacementGrid[x.Key] != UncertainBool.FALSE).Count() == _riskGrid[index])
            {
                //If a clicked tile with the number x has exactly one empty square around it (minus the tiles marked as dangerous), then we know there�s a mine there.
                foreach (KeyValuePair<int, int> kvp in neighbourRisk.Where(x => x.Value == -1).Where(x => _estimatedDangersPlacementGrid[x.Key] != UncertainBool.FALSE)) _estimatedDangersPlacementGrid[kvp.Key] = UncertainBool.TRUE;
            }

            if (neighbourRisk.Where(x => x.Value == -1).Where(x => _estimatedDangersPlacementGrid[x.Key] == UncertainBool.TRUE).Count() != neighbourRisk.Where(x => x.Value == -1).Count() &&
                neighbourRisk.Where(x => _estimatedDangersPlacementGrid[x.Key] == UncertainBool.TRUE).Count() == _riskGrid[index])
            {
                //If a clicked tile with a number x has exactly x tile(s) marked as dangerous, we know every other tile around it is safe and cannot be mines.
                foreach (KeyValuePair<int, int> kvp in neighbourRisk.Where(x => x.Value == -1).Where(x => _estimatedDangersPlacementGrid[x.Key] != UncertainBool.TRUE)) _estimatedDangersPlacementGrid[kvp.Key] = UncertainBool.FALSE;
            }
        }
    }

    void ClickBox(int gridIndex)
    {
        PlayBoard._grid[gridIndex].OnClick(false);
    }
}