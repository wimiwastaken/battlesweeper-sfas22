using System.Collections;
using System.Linq;
using UnityEngine;
using UnityEngine.EventSystems;

public class Game : MonoBehaviour
{
    public Board P1Board, P2Board;
    public Board P1SetupBoard, P2SetupBoard;
    public UI UserInterface;
    public AI AIPlayer;
    public RectTransform FlagParent;
    public AudioSource ClickSound;

    internal float _maxTime;
    internal float _timeLeft;
    internal EventSystem _eventSystem;
    public bool _player1sTurn = true;
    internal bool _confuseMode;

    private int _p1ConfusesLeft, _p2ConfusesLeft;
    private bool _gameInProgress;

    public void OnClickedNewGame(bool againstAI = false)
    {
        StartCoroutine(SetupGame(againstAI));
    }

    public void OnClickedExit()
    {
#if !UNITY_EDITOR
        Application.Quit();
#endif
    }

    public void OnClickedHelp()
    {
        if (UserInterface != null)
        {
            UserInterface.HideMenu();
            UserInterface.ShowHelp();
        }
    }

    public void OnClickedBack()
    {
        if (UserInterface != null)
        {
            UserInterface.HideHelp();
            UserInterface.ShowMenu();
        }
    }

    public void OnClickedReset()
    {
        if (P1Board != null)
        {
            P1Board.Clear();
        }

        if (UserInterface != null)
        {
            UserInterface.HideResult();
            UserInterface.ShowMenu();
        }
    }

    private IEnumerator SetupGame(bool againstAI)
    {
        StartCoroutine(P1SetupBoard.CreateBoard());
        StartCoroutine(P2SetupBoard.CreateBoard());
        StartCoroutine(P1Board.CreateBoard());
        StartCoroutine(P2Board.CreateBoard());
        yield return new WaitUntil(() => P1Board._grid != null);

        if (P1Board != null) P1Board.Setup(BoardEvent);
        if (P2Board != null) P2Board.Setup(BoardEvent);
        if (P1SetupBoard != null) P1SetupBoard.Setup(BoardEvent);
        if (P2SetupBoard != null) P2SetupBoard.Setup(BoardEvent);
        yield return null;

        if (P1SetupBoard != null)
        {
            P1SetupBoard.PlaceDangers(againstAI);
        }
        if (P2SetupBoard != null)
        {
            P2SetupBoard.PlaceDangers(false);
            if (againstAI)
            {
                AIPlayer.PlayBoard = P2Board;
                P2Board._aiBoard = true;
            }
            else
            {
                AIPlayer.PlayBoard = null;
                P2Board._aiBoard = false;
            }
        }

        if (UserInterface != null)
        {
            UserInterface.HideMenu();
            UserInterface.ShowSetup();
            UserInterface.UpdateMineCount(1, P1SetupBoard._dangersLeft);
            UserInterface.UpdateMineCount(2, P2SetupBoard._dangersLeft);
        }
    }

    private void OnFinishedSetup()
    {
        if (P1Board != null)
        {
            if (P1SetupBoard != null) P1Board._dangers = P1SetupBoard._dangers;
            P1Board.RechargeBoxes();
            P1Board._player1Board = true;
        }
        if (P2Board != null)
        {
            if (P2SetupBoard != null) P2Board._dangers = P2SetupBoard._dangers;
            if (AIPlayer.PlayBoard != null)
            {
                AIPlayer.CalculateRiskGrid(true);
            }
            P2Board._player1Board = false;
            P2Board.RechargeBoxes();
        }

        if (UserInterface != null)
        {
            _player1sTurn = true;
            P1Board._currentTurn = _player1sTurn;
            P2Board._currentTurn = !_player1sTurn;

            P1Board.FadeActive(P1Board._currentTurn);
            P2Board.FadeActive(P2Board._currentTurn);

            UserInterface.HideSetup();
            UserInterface.ShowGame();
            UserInterface.UpdatePlayerTurn(_player1sTurn);
            _gameInProgress = true;

            if (_maxTime > 0)
            {
                _timeLeft = _maxTime;
            }
        }
    }

    public void SetGridSize(int size)
    {
        P1Board._gridHeight = P1Board._gridWidth = size;
        P2Board._gridHeight = P2Board._gridWidth = size;
        P1SetupBoard._gridHeight = P1SetupBoard._gridWidth = size;
        P2SetupBoard._gridHeight = P2SetupBoard._gridWidth = size;
    }

    public void SetConfuseMode(bool flag)
    {
        if (!flag) 
        {
            _p1ConfusesLeft = 0;
            _p2ConfusesLeft = 0;
            UserInterface.ConfusesLeftPlayer1.gameObject.SetActive(false);
            UserInterface.ConfusesLeftPlayer2.gameObject.SetActive(false);
        }
        else
        {
            _p1ConfusesLeft = 5;
            _p2ConfusesLeft = 5;
            UserInterface.ConfusesLeftPlayer1.gameObject.SetActive(true);
            UserInterface.ConfusesLeftPlayer2.gameObject.SetActive(true);
        }
        _confuseMode = flag;
    }

    public void SetMineAmount(int amount)
    {
        P1Board._dangerousBoxAmount = amount;
        P2Board._dangerousBoxAmount = amount;
        P1SetupBoard._dangerousBoxAmount = amount;
        P2SetupBoard._dangerousBoxAmount = amount;
    }

    public void SetTurnTimerActive(float time)
    {
        if (time == 0) UserInterface.TimerText.gameObject.SetActive(false);
        else UserInterface.TimerText.gameObject.SetActive(true);
        _maxTime = time;
    }

    public void ConfuseOpponent(int player)
    {
        if (!P1Board._confusedTurn && !P2Board._confusedTurn)
        {
            if (_player1sTurn && player % 2 == 1 && _p1ConfusesLeft > 0)
            {
                P1Board.FadeActive(false);
                P2Board.FadeActive(true);
                P1Board._confusedTurn = true;
                P2Board._confusedTurn = true;
                _p1ConfusesLeft--;
                UserInterface.UpdateConfuseCount(1, _p1ConfusesLeft);
            }
            else if (!_player1sTurn && player % 2 == 0 && _p2ConfusesLeft > 0)
            {
                P1Board.FadeActive(true);
                P2Board.FadeActive(false);
                P1Board._confusedTurn = true;
                P2Board._confusedTurn = true;
                _p2ConfusesLeft--;
                UserInterface.UpdateConfuseCount(2, _p2ConfusesLeft);
            }
        }
        else
        {
            if (_player1sTurn && player % 2 == 1)
            {
                P1Board.FadeActive(true);
                P2Board.FadeActive(false);
                
                P1Board._confusedTurn = false;
                P2Board._confusedTurn = false;
                _p1ConfusesLeft++;
                UserInterface.UpdateConfuseCount(1, _p1ConfusesLeft);
            }
            else if (!_player1sTurn && player % 2 == 0)
            {
                P1Board.FadeActive(false);
                P2Board.FadeActive(true);

                P1Board._confusedTurn = false;
                P2Board._confusedTurn = false;
                _p2ConfusesLeft++;
                UserInterface.UpdateConfuseCount(2, _p2ConfusesLeft);
            }
        }
    }

    private void Awake()
    {
        _gameInProgress = false;
        _eventSystem = EventSystem.current;
    }

    private void Update()
    {
        if (_maxTime > 0 && _gameInProgress)
        {
            UserInterface.TimeLeft = _timeLeft = Mathf.Clamp(_timeLeft - Time.deltaTime, 0, Mathf.Infinity);
            if (_timeLeft <= 0)
            {
                _gameInProgress = false;
                StartCoroutine(GameOver(WinReason.NO_TIME));
            }
        }
    }

    private void BoardEvent(Board.Event eventType)
    {
        if (eventType != Board.Event.Setup)
        {
            if ((eventType == Board.Event.Confused || eventType == Board.Event.ClickedBlank || eventType == Board.Event.ClickedNearDanger) && UserInterface != null)
            {
                _player1sTurn = !_player1sTurn;
                _timeLeft = _maxTime;

                P1Board._currentTurn = _player1sTurn;
                P2Board._currentTurn = !_player1sTurn;

                P1Board.FadeActive(P1Board._currentTurn);
                P2Board.FadeActive(P2Board._currentTurn);

                UserInterface.UpdatePlayerTurn(_player1sTurn);
            }

            if (eventType == Board.Event.ClickedDanger && UserInterface != null)
            {
                foreach (Transform child in FlagParent) Destroy(child.gameObject);

                _gameInProgress = false;
                StartCoroutine(GameOver(WinReason.MINE_PRESSED));
            }

            if (eventType == Board.Event.Win && UserInterface != null)
            {
                foreach (Transform child in FlagParent) Destroy(child.gameObject);

                _gameInProgress = false;
                StartCoroutine(GameOver(WinReason.EMPTIED_BOARD));
            }

            StartCoroutine(ResetConfusion(eventType));
        }
        else if(P1SetupBoard._dangersLeft <= 0 && P2SetupBoard._dangersLeft <= 0)
        {
            OnFinishedSetup();
        }
        else
        {
            if (P1SetupBoard._dangersLeft <= 0) foreach (Box box in P1SetupBoard._grid) box._button.interactable = false;
            if (P2SetupBoard._dangersLeft <= 0) foreach (Box box in P2SetupBoard._grid) box._button.interactable = false;

            UserInterface.UpdateMineCount(1, P1SetupBoard._dangersLeft);
            UserInterface.UpdateMineCount(2, P2SetupBoard._dangersLeft);
        }
    }

    private IEnumerator GameOver(WinReason reason)
    {
        if (_player1sTurn)
        {
            foreach (Box box in P1Board._grid.Where(x => x.IsDangerous))
            {
                box.MineReveal();
                yield return new WaitForSeconds(0.1f);
            }
        }
        else
        {
            foreach (Box box in P2Board._grid.Where(x => x.IsDangerous))
            {
                box.MineReveal();
                yield return new WaitForSeconds(0.1f);
            }
        }
        yield return new WaitForSeconds(0.5f);
        UserInterface.HideGame();
        UserInterface.ShowResult(reason != WinReason.EMPTIED_BOARD ? !_player1sTurn : _player1sTurn, reason);
        _p1ConfusesLeft = 5;
        _p2ConfusesLeft = 5;

        UserInterface.UpdateConfuseCount(1, _p1ConfusesLeft);
        UserInterface.UpdateConfuseCount(2, _p2ConfusesLeft);
    }

    private IEnumerator ResetConfusion(Board.Event eventType)
    {
        yield return new WaitForEndOfFrame();
        P1Board._confusedTurn = false;
        P2Board._confusedTurn = false;

        if (eventType != Board.Event.ClickedDanger && eventType != Board.Event.Win && _gameInProgress && !_player1sTurn && AIPlayer.PlayBoard != null)
        {
            AIPlayer.CalculateRiskGrid();
            StartCoroutine(AIPlayer.PlayTurn());
        }
    }
}
