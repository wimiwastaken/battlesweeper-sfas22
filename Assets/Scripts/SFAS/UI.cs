using UnityEngine;
using TMPro;
using System.Collections;

public enum WinReason
{
    MINE_PRESSED, EMPTIED_BOARD, NO_TIME
}

public class UI : MonoBehaviour
{
    [SerializeField] private CanvasGroup Menu;
    [SerializeField] private CanvasGroup Help;
    [SerializeField] private CanvasGroup Setup;
    [SerializeField] private CanvasGroup Game;
    [SerializeField] private CanvasGroup Result;
    [Space]
    [SerializeField] private TMP_Text ResultText;
    [SerializeField] private TMP_Text ResultReasonText;
    [SerializeField] private TMP_Text TurnText;
    [SerializeField] private TMP_Text BoxesLeftPlayer1, BoxesLeftPlayer2;
    [SerializeField] internal TMP_Text TimerText;
    [SerializeField] internal TMP_Text ConfusesLeftPlayer1, ConfusesLeftPlayer2;

    private static readonly string[] PlayerColours = { "#CFC415", "#CF153F" };
    private static readonly string[] ResultTexts = { "Player <color={0}>1</color> Wins", "Player <color={0}>2</color> Wins" };
    private static readonly string TimeLeftText = "Time Left: {0}";

    private static readonly string[] ResultReasonTexts_MinePressed = { "Player <color={0}>1</color> pressed a mine", "Player <color={0}>2</color> pressed a mine" };
    private static readonly string[] ResultReasonTexts_EmptiedBoard = { "Player <color={0}>1</color> cleared their board", "Player <color={0}>2</color> cleared their board" };
    private static readonly string[] ResultReasonTexts_NoTime = { "Player <color={0}>1</color> ran out of time", "Player <color={0}>2</color> ran out of time" };

    private static readonly string[] TurnTexts = { "Player <color={0}>1</color>'s Turn", "Player <color={0}>2</color>'s Turn" };
    private static readonly string[] SetupTexts = { "Player <color={0}>1</color>\n{1} mines left", "Player <color={0}>2</color>\n{1} mines left" };
    private static readonly float AnimationTime = 0.5f;

    internal float TimeLeft;

    public void ShowMenu() => StartCoroutine(ShowCanvas(Menu, 1.0f));
    public void ShowHelp() => StartCoroutine(ShowCanvas(Help, 1.0f));
    public void ShowSetup() => StartCoroutine(ShowCanvas(Setup, 1.0f));
    public void ShowGame() => StartCoroutine(ShowCanvas(Game, 1.0f));
    public void ShowResult(bool player1Victory, WinReason winReason)
    {
        if (ResultText != null)
        {
            ResultText.text = string.Format(ResultTexts[player1Victory ? 0 : 1], PlayerColours[player1Victory ? 0 : 1]);
        }

        if (ResultReasonText != null)
        {
            switch (winReason)
            {
                case WinReason.MINE_PRESSED:
                    ResultReasonText.text = string.Format(ResultReasonTexts_MinePressed[!player1Victory ? 0 : 1], PlayerColours[!player1Victory ? 0 : 1]);
                    break;
                case WinReason.EMPTIED_BOARD:
                    ResultReasonText.text = string.Format(ResultReasonTexts_EmptiedBoard[player1Victory ? 0 : 1], PlayerColours[player1Victory ? 0 : 1]);
                    break;
                case WinReason.NO_TIME:
                    ResultReasonText.text = string.Format(ResultReasonTexts_NoTime[!player1Victory ? 0 : 1], PlayerColours[!player1Victory ? 0 : 1]);
                    break;
            }
        }

        StartCoroutine(ShowCanvas(Result, 1.0f));
    }

    public void HideMenu() => StartCoroutine(ShowCanvas(Menu, 0.0f));
    public void HideHelp() => StartCoroutine(ShowCanvas(Help, 0.0f));
    public void HideSetup() => StartCoroutine(ShowCanvas(Setup, 0.0f));
    public void HideGame() => StartCoroutine(ShowCanvas(Game, 0.0f));
    public void HideResult() => StartCoroutine(ShowCanvas(Result, 0.0f));

    public void UpdatePlayerTurn(bool player1sTurn)
    {
        TurnText.text = string.Format(TurnTexts[player1sTurn ? 0 : 1], PlayerColours[player1sTurn ? 0 : 1]);
        StartCoroutine(PingPongTextAlpha());

        IEnumerator PingPongTextAlpha()
        {
            StartCoroutine(ChangeTextAlpha(1f));
            yield return new WaitForSeconds(1.5f);
            StartCoroutine(ChangeTextAlpha(0f));

            IEnumerator ChangeTextAlpha(float target)
            {
                float startAlpha = TurnText.alpha;
                float t = 0.0f;

                while (t < AnimationTime)
                {
                    t = Mathf.Clamp(t + Time.deltaTime, 0.0f, AnimationTime);
                    TurnText.alpha = Mathf.SmoothStep(startAlpha, target, t / AnimationTime);
                    yield return null;
                }
            }
        }
    }

    public void UpdateMineCount(int player, int boxesLeft)
    {
        switch(player % 2)
        {
            case 0:
                BoxesLeftPlayer1.text = string.Format(SetupTexts[player % 2], PlayerColours[player % 2], boxesLeft);
                break;
            case 1:
                BoxesLeftPlayer2.text = string.Format(SetupTexts[player % 2], PlayerColours[player % 2], boxesLeft);
                break;
        }
    }

    public void UpdateConfuseCount(int player, int confusesLeft)
    {
        switch(player % 2)
        {
            case 0:
                ConfusesLeftPlayer2.text = string.Format("Question Tiles: {0}", confusesLeft);
                break;
            case 1:
                ConfusesLeftPlayer1.text = string.Format("Question Tiles: {0}", confusesLeft);
                break;
        }
    }

    private void Awake()
    {
        if (Menu != null)
        {
            Menu.alpha = 0.0f;
            Menu.interactable = false;
            Menu.blocksRaycasts = false;
        }

        if (Help != null)
        {
            Help.alpha = 0.0f;
            Help.interactable = false;
            Help.blocksRaycasts = false;
        }

        if (Setup != null)
        {
            Setup.alpha = 0.0f;
            Setup.interactable = false;
            Setup.blocksRaycasts = false;
        }

        if (Game != null)
        {
            Game.alpha = 0.0f;
            Game.interactable = false;
            Game.blocksRaycasts = false;
        }

        if (Result != null)
        {
            Result.alpha = 0.0f;
            Result.interactable = false;
            Result.blocksRaycasts = false;
        }
    }

    private void Update()
    {
        TimerText.text = string.Format(TimeLeftText, TimeLeft.ToString("00"));
    }

    private IEnumerator ShowCanvas(CanvasGroup group, float target)
    {
        if (group != null)
        {
            float startAlpha = group.alpha;
            float t = 0.0f;

            group.interactable = target >= 1.0f;
            group.blocksRaycasts = target >= 1.0f;

            while (t < AnimationTime)
            {
                t = Mathf.Clamp(t + Time.deltaTime, 0.0f, AnimationTime);
                group.alpha = Mathf.SmoothStep(startAlpha, target, t / AnimationTime);
                yield return null;
            }
        }
    }
}
