using System;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using DG.Tweening;

public class Box : MonoBehaviour
{
    [SerializeField] private Color[] DangerColors = new Color[8];
    [SerializeField] private Image Danger;
    [SerializeField] private GameObject FlagPrefab;

    private TMP_Text _textDisplay;
    private Action<Box> _changeCallback;

    internal Button _button;
    internal Board _board;
    internal GameObject _flag;

    public int RowIndex { get; private set; }
    public int ColumnIndex { get; private set; }
    public int ID { get; private set; }
    public int DangerNearby { get; private set; }
    public bool IsDangerous { get; private set; }
    public bool IsFlagged { get; private set; }
    public bool IsUnknown { get; private set; }
    public bool IsActive { get { return _button != null && _button.interactable; } }

    public void Setup(int id, int row, int column)
    {
        ID = id;
        RowIndex = row;
        ColumnIndex = column;
    }

    public void Charge(int dangerNearby, bool danger, Action<Box> onChange, bool softReset = false)
    {
        _changeCallback = onChange;
        DangerNearby = dangerNearby;
        IsDangerous = danger;
        ResetState(softReset);
    }

    public void ChargeSetup(Action<Box> onChange)
    {
        _changeCallback = onChange;
        ResetState();
    }

    public void Reveal()
    {
        if (_button != null)
        {
            _button.interactable = false;
        }

        if (_textDisplay != null)
        {
            _textDisplay.enabled = true;
        }
    }

    public void StandDown()
    {
        if (_button != null)
        {
            _button.interactable = false;
        }

        if (Danger != null)
        {
            Danger.enabled = false;
        }

        if (_textDisplay != null)
        {
            _textDisplay.enabled = false;
        }
    }

    public void OnClick(bool tilePress)
    {
        if (!_board._aiBoard || (_board._aiBoard && (!tilePress || _board._confusedTurn)))
        {
            if ((_board._currentTurn != _board._confusedTurn || _board.SetupBoard) && !IsFlagged)
            {
                if (_board._confusedTurn)
                {
                    IsUnknown = true;
                    if (!IsDangerous && _textDisplay != null)
                    {
                        _textDisplay.text = "?";
                        _textDisplay.color = DangerColors[DangerColors.Length - 1];
                    }
                }
                else if (!_board._aiBoard || (_board._aiBoard && _board._currentTurn))
                {
                    if (!_board._confusedTurn && (_board._currentTurn || _board.SetupBoard))
                    {
                        if (_button != null)
                        {
                            _button.interactable = false;
                        }

                        if (IsDangerous && Danger != null)
                        {
                            Danger.enabled = true;
                        }
                        else if (_textDisplay != null)
                        {
                            _textDisplay.enabled = true;
                        }

                        if (_flag != null) Destroy(_flag.gameObject);

                    }
                }
                _changeCallback?.Invoke(this);
            }
        }
    }

    public void MineReveal()
    {
        if (_button != null)
        {
            _button.interactable = false;
        }

        if (IsDangerous && Danger != null)
        {
            if(!Danger.enabled) Danger.DOFade(1f, 0.1f).From(0f);
            Danger.enabled = true;
        }
        else if (_textDisplay != null)
        {
            _textDisplay.enabled = true;
        }

        if (_flag != null) Destroy(_flag.gameObject);
    }

    internal void MarkFlag()
    {
        IsFlagged = !IsFlagged;
        switch(IsFlagged)
        {
            case true:
                _flag = Instantiate(FlagPrefab, _board.FlagParent);
                _flag.transform.position = transform.position;
                _flag.transform.rotation = transform.rotation;
                _flag.GetComponent<RectTransform>().sizeDelta = GetComponent<RectTransform>().sizeDelta;
                _flag.GetComponent<Image>().color = _board._player1Board ? new Color(0.80859375f, 0.765625f, 0.08203125f) : new Color(0.80859375f, 0.08203125f, 0.24609375f);
                break;
            case false:
                Destroy(_flag.gameObject);
                break;
        }
    }

    private void Awake()
    {
        _textDisplay = GetComponentInChildren<TMP_Text>(true);
        _button = GetComponent<Button>();
        _button.onClick.AddListener(() => OnClick(true));
        _button.onClick.AddListener(() => FindObjectOfType<Game>().ClickSound.Play());

        ResetState();
    }

    private void ResetState(bool softReset = false)
    {
        if (Danger != null)
        {
            Danger.enabled = false;
        }

        if (_textDisplay != null)
        {
            if (DangerNearby > 0)
            {
                _textDisplay.text = DangerNearby.ToString("D");
                _textDisplay.color = DangerColors[DangerNearby-1];
            }
            else
            {
                _textDisplay.text = string.Empty;
            }

            if(!softReset) _textDisplay.enabled = false;
            else _textDisplay.enabled = true;
        }

        if (_button != null)
        {
            if(!softReset) _button.interactable = true;
            else _button.interactable = false;
        }
    }
}
