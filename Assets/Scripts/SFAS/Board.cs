using DG.Tweening;

using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

using UnityEngine;

public class Board : MonoBehaviour
{
    public enum Event { ClickedBlank, ClickedNearDanger, ClickedDanger, Win, Confused, Setup }

    [SerializeField] private Box BoxPrefab;
    public Transform FlagParent;
    [Space]
    public bool SetupBoard;

    internal int _gridWidth = 10;
    internal int _gridHeight = 10;
    internal int _dangerousBoxAmount = 10;
    internal Box[] _grid;
    internal bool _currentTurn;
    internal bool _confusedTurn;
    internal int _dangersLeft;
    internal List<bool> _dangers;
    internal bool _player1Board;
    internal bool _aiBoard;

    private Vector2Int[] _neighbours;
    private RectTransform _rect;
    private Action<Event> _clickEvent;

    public void Setup(Action<Event> onClickEvent)
    {
        _clickEvent = onClickEvent;
        Clear();
    }

    public void Clear()
    {
        for (int row = 0; row < _gridHeight; ++row)
        {
            for (int column = 0; column < _gridWidth; ++column)
            {
                int index = row * _gridWidth + column;
                _grid[index].StandDown();
            }
        }
    }

    public void PlaceDangers(bool randomPlacement)
    {
        int numberOfItems = _gridWidth * _gridHeight;
        _dangers = new List<bool>(numberOfItems);

        if (randomPlacement)
        {
            for (int count = 0; count < numberOfItems; ++count)
            {
                _dangers.Add(count < _dangerousBoxAmount);
            }

            _dangers.RandomShuffle();
        }
        else
        {
            _dangersLeft = _dangerousBoxAmount;
            for (int count = 0; count < numberOfItems; ++count)
            {
                _dangers.Add(false);
            }

            for (int row = 0; row < _gridHeight; ++row)
            {
                for (int column = 0; column < _gridWidth; ++column)
                {
                    int index = row * _gridWidth + column;
                    _grid[index].ChargeSetup(OnClickedSetupBox);
                }
            }
        }
    }

    public void RandomiseMinePlacement()
    {
        while (_dangersLeft > 0)
        {
            int index = UnityEngine.Random.Range(0, _grid.Length);
            if (!_dangers[index])
            {
                _grid[index].OnClick(false);
            }
        }
    }

    public void RechargeBoxes()
    {
        List<bool> dangerList = _dangers;
        
        for (int row = 0; row < _gridHeight; ++row)
        {
            for (int column = 0; column < _gridWidth; ++column)
            {
                int index = row * _gridWidth + column;
                _grid[index].Charge(CountDangerNearby(dangerList, index), dangerList[index], OnClickedBox);
            }
        }
    }

    public IEnumerator CreateBoard()
    {
        yield return null;
        foreach(Transform child in transform)
        {
            if(transform.GetChild(0) != child) Destroy(child.gameObject);
        }
        _grid = new Box[_gridWidth * _gridHeight];
        _rect = GetComponent<RectTransform>();
        Vector2 boxRectSizeDelta = _rect.sizeDelta / _gridWidth;

        Vector2 startPosition = -(_rect.sizeDelta * 0.5f) + (boxRectSizeDelta * 0.5f);
        startPosition.y *= -1.0f;

        _neighbours = new Vector2Int[8]
        {
            new Vector2Int(-_gridWidth - 1, -1),
            new Vector2Int(-_gridWidth, -1),
            new Vector2Int(-_gridWidth + 1, -1),
            new Vector2Int(-1, 0),
            new Vector2Int(1, 0),
            new Vector2Int(_gridWidth - 1, 1),
            new Vector2Int(_gridWidth, 1 ),
            new Vector2Int(_gridWidth + 1, 1)
        };

        for (int row = 0; row < _gridWidth; ++row)
        {
            GameObject rowObj = new GameObject(string.Format("Row{0}", row), typeof(RectTransform));
            RectTransform rowRect = rowObj.transform as RectTransform;
            rowRect.SetParent(transform);
            rowRect.anchoredPosition = new Vector2(0, startPosition.y - (boxRectSizeDelta.y * row));
            rowRect.sizeDelta = new Vector2(boxRectSizeDelta.x * _gridWidth, boxRectSizeDelta.y);
            rowRect.localScale = Vector2.one;

            for (int column = 0; column < _gridHeight; ++column)
            {
                int index = row * _gridWidth + column;
                _grid[index] = Instantiate(BoxPrefab, rowObj.transform);
                _grid[index]._board = this;
                _grid[index].Setup(index, row, column);
                RectTransform gridBoxTransform = _grid[index].transform as RectTransform;
                _grid[index].name = string.Format("ID{0}, Row{1}, Column{2}", index, row, column);
                gridBoxTransform.sizeDelta = boxRectSizeDelta;
                gridBoxTransform.anchoredPosition = new Vector2( startPosition.x + (boxRectSizeDelta.x * column), 0.0f);
            }

            rowRect.localEulerAngles = Vector3.zero;
            rowRect.anchoredPosition3D = new Vector3(rowRect.anchoredPosition3D.x, rowRect.anchoredPosition3D.y, 0f);
        }

        // Sanity check
        for(int count = 0; count < _grid.Length; ++count)
        {
            //Debug.LogFormat("Count: {0}  ID: {1}  Row: {2}  Column: {3}", count, _grid[count].ID, _grid[count].RowIndex, _grid[count].ColumnIndex);
        }
    }

    internal void FadeActive(bool active)
    {
        foreach(Box box in _grid)
        {
            box._button.image.DOColor(active ? box._button.colors.normalColor : box._button.colors.disabledColor, 0.25f);
        }
    }

    private int CountDangerNearby(List<bool> danger, int index)
    {
        int result = 0;
        int boxRow = index / _gridWidth;

        if (!danger[index])
        {
            for (int count = 0; count < _neighbours.Length; ++count)
            {
                int neighbourIndex = index + _neighbours[count].x;
                int expectedRow = boxRow + _neighbours[count].y;
                int neighbourRow = neighbourIndex / _gridWidth;
                result += (expectedRow == neighbourRow && neighbourIndex >= 0 && neighbourIndex < danger.Count && danger[neighbourIndex]) ? 1 : 0;
            }
        }

        return result;
    }

    private void OnClickedBox(Box box)
    {
        bool firstRound = CheckForFirst(box.ID);

        Event clickEvent = Event.ClickedBlank;

        if (box.IsDangerous)
        {
            if (!_confusedTurn)
            {
                if (firstRound)
                {
                    _dangers[box.ID] = false;
                    while (_dangers.Where(x => x == true).Count() < _dangerousBoxAmount)
                    {
                        int randomTile = UnityEngine.Random.Range(0, _dangers.Count);
                        if (!_dangers[randomTile])
                        {
                            _dangers[randomTile] = true;

                            _grid[randomTile].Charge(CountDangerNearby(_dangers, randomTile), _dangers[randomTile], OnClickedBox);
                            for (int count = 0; count < _neighbours.Length; ++count)
                            {
                                int neighbourIndex = randomTile + _neighbours[count].x;
                                int expectedRow = (randomTile / _gridWidth) + _neighbours[count].y;
                                int neighbourRow = neighbourIndex / _gridWidth;
                                if (expectedRow == neighbourRow && neighbourIndex >= 0 && neighbourIndex < _dangers.Count)
                                    _grid[neighbourIndex].Charge(CountDangerNearby(_dangers, neighbourIndex), _dangers[neighbourIndex], OnClickedBox);
                            }
                        }
                    }

                    box.Charge(CountDangerNearby(_dangers, box.ID), _dangers[box.ID], OnClickedBox, true);
                    for (int count = 0; count < _neighbours.Length; ++count)
                    {
                        int neighbourIndex = box.ID + _neighbours[count].x;
                        int expectedRow = (box.ID / _gridWidth) + _neighbours[count].y;
                        int neighbourRow = neighbourIndex / _gridWidth;
                        if (expectedRow == neighbourRow && neighbourIndex >= 0 && neighbourIndex < _dangers.Count)
                            _grid[neighbourIndex].Charge(CountDangerNearby(_dangers, neighbourIndex), _dangers[neighbourIndex], OnClickedBox);
                    }

                    if (box.DangerNearby > 0)
                    {
                        clickEvent = Event.ClickedNearDanger;
                    }
                    else
                    {
                        ClearNearbyBlanks(box);
                    }
                }
                else
                {
                    clickEvent = Event.ClickedDanger;
                }
            }
            else clickEvent = Event.Confused;
        }
        else if (box.IsUnknown)
        {
            clickEvent = Event.Confused;
        }
        else if (box.DangerNearby > 0)
        {
            clickEvent = Event.ClickedNearDanger;
        }
        else
        {
            ClearNearbyBlanks(box);
        }

        if (CheckForWin())
        {
            clickEvent = Event.Win;
        }

        _clickEvent?.Invoke(clickEvent);

    }

    private void OnClickedSetupBox(Box box)
    {
        if (_dangersLeft > 0)
        {
            _dangers[box.ID] = true;
            _dangersLeft--;
        }

        _clickEvent?.Invoke(Event.Setup);
    }

    private bool CheckForFirst(int indexPressed)
    {
        bool result = true;
        for (int count = 0; result && count < _grid.Length; ++count)
        {
            if (!_grid[count].IsActive && count != indexPressed)
            {
                result = false;
                break;
            }
        }

        return result;
    }

    private bool CheckForWin()
    {
        bool Result = true;

        for( int count = 0; Result && count < _grid.Length; ++count)
        {
            if(!_grid[count].IsDangerous && _grid[count].IsActive)
            {
                Result = false;
                break;
            }
        }

        return Result;
    }

    private void ClearNearbyBlanks(Box box)
    {
        RecursiveClearBlanks(box);
    }

    private void RecursiveClearBlanks(Box box)
    {
        if (!box.IsDangerous)
        {
            box.Reveal();

            if (box.DangerNearby == 0)
            {
                for (int count = 0; count < _neighbours.Length; ++count)
                {
                    int neighbourIndex = box.ID + _neighbours[count].x;
                    int expectedRow = box.RowIndex + _neighbours[count].y;
                    int neighbourRow = neighbourIndex / _gridWidth;
                    bool correctRow = expectedRow == neighbourRow;
                    bool active = neighbourIndex >= 0 && neighbourIndex < _grid.Length && _grid[neighbourIndex].IsActive;

                    if (correctRow && active)
                    {
                        RecursiveClearBlanks(_grid[neighbourIndex]);
                    }
                }
            }
        }
    }
}
