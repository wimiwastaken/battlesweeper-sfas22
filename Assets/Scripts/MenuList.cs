using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;

public class MenuList : MonoBehaviour
{
    [Serializable]
    public struct Option
    {
        [TextArea]
        public string name;
        public UnityEvent action;
    }

    public TMP_Text ValueText;
    public int StartOptionIndex;
    public Option[] Options;

    private int _currentOptionIndex = 0;

    private void Awake()
    {
        _currentOptionIndex = StartOptionIndex;
    }

    public void OnForwardButtonPress()
    {
        _currentOptionIndex++;
        _currentOptionIndex %= Options.Length;

        ValueText.text = Options[_currentOptionIndex].name;
        if(Options[_currentOptionIndex].action != null) Options[_currentOptionIndex].action.Invoke();
    }

    public void OnBackwardButtonPress()
    {
        _currentOptionIndex--;
        if (_currentOptionIndex < 0) _currentOptionIndex = Options.Length - 1;
        _currentOptionIndex %= Options.Length;

        ValueText.text = Options[_currentOptionIndex].name;
        if (Options[_currentOptionIndex].action != null) Options[_currentOptionIndex].action.Invoke();
    }
}
