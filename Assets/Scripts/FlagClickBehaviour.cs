using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;

[RequireComponent(typeof(Box))]
public class FlagClickBehaviour : MonoBehaviour, IPointerClickHandler
{
    private Box _box;

    private void Awake()
    {
        _box = GetComponent<Box>();
    }

    public void OnPointerClick(PointerEventData eventData)
    {
        if (_box._board._currentTurn && !_box._board.SetupBoard && (_box._board._player1Board || FindObjectOfType<AI>().PlayBoard == null) && _box.IsActive)
        {
            if (eventData.button == PointerEventData.InputButton.Right)
            {
                _box.MarkFlag();
            }
        }
    }
}
